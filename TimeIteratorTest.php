<?php
namespace Avris\Helper;

use ICanBoogie\DateTime;

class TimeIteratorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provider
     */
    public function testIterator($start, $step, $stop, $result)
    {
        $actual = [];
        foreach (TimeIterator::create($start, $step, $stop) as $datetime) {
            $this->assertInstanceOf('ICanBoogie\DateTime', $datetime);
            $actual[] = $datetime->format('Y-m-d H:i:s');
        }
        $this->assertEquals($result, $actual);
    }

    public function provider()
    {
        return [
            ['2015-01-01', '+1 month', '2015-06-30', [
                '2015-01-01 00:00:00',
                '2015-02-01 00:00:00',
                '2015-03-01 00:00:00',
                '2015-04-01 00:00:00',
                '2015-05-01 00:00:00',
                '2015-06-01 00:00:00',
            ]],
            [new DateTime('2013-11-19 19:30'), '-1 hour', new DateTime('2013-11-19 17:00'), [
                '2013-11-19 19:30:00',
                '2013-11-19 18:30:00',
                '2013-11-19 17:30:00',
            ]],
        ];
    }

    public function testGetArray()
    {
        $array = TimeIterator::create('2015-11-19', '+1 day', '2015-11-20')->getArray();
        $this->assertCount(2, $array);
        $this->assertInstanceOf('ICanBoogie\DateTime', $array[0]);
        $this->assertInstanceOf('ICanBoogie\DateTime', $array[1]);
        $this->assertEquals('2015-11-19', $array[0]->format('Y-m-d'));
        $this->assertEquals('2015-11-20', $array[1]->format('Y-m-d'));
    }

    public function testFormat()
    {
        $array = TimeIterator::create('2015-11-19', '+1 day', '2015-11-20')->format('Y-m-d');
        $this->assertEquals(['2015-11-19', '2015-11-20'], $array);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testBlockInfiniteLoop()
    {
        TimeIterator::create('2015-01-01', '+1 day', '2014-12-01');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testBlockInfiniteLoopReversed()
    {
        TimeIterator::create('2015-01-01', '-1 day', '2015-02-01');
    }
}