## Avris TimeIterator ##

Simple helper to iterate over time:

    foreach (TimeIterator::create('today', '+1 day', 'last day of this month') as $day) {
        createEventForDate($day);
        echo 'Event created for date: ' . $day->format('Y-m-d') . PHP_EOL;
    }
    
will output a list of dates from today until the end of the month. Simple, isn't it?

You can also fetch an array of `DateTime` objects using `getArray()`:

    $ti = new TimeIterator('now', '+1 minute', 'tomorrow');
    doSomething($ti->getArray());
    
Or if you just need an array of strings, you can just use `format($format)`:

    $ti = new TimeIterator('15:00', '-5 minutes', '14:30');
    return $ti->format('H:i');
    
will return:

    [
        '15:00',
        '14:55',
        '14:50',
        '14:45',
        '14:40',
        '14:35',
        '14:30',        
    ]

### Author ###

* ![Foto](http://avris.it/gfx/favicon.png)&nbsp;**Andrzej Prusinowski** [Avris.it](http://avris.it)
* ![Favicon](http://avris.it/dl/cc.png)&nbsp;Licence: [CC-BY](http://creativecommons.org/licenses/by/3.0/pl/)