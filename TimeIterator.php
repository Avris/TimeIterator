<?php
namespace Avris\Helper;

use ICanBoogie\DateTime;

class TimeIterator implements \Iterator
{
    /** @var DateTime */
    private $start;

    /** @var string */
    private $step;

    /** @var DateTime */
    private $stop;

    /** @var DateTime */
    private $iterator;

    /** @var integer */
    private $i;

    /** @var  boolean */
    private $isInversed;

    /**
     * @param string|DateTime $start
     * @param string $step
     * @param string|DateTime $stop
     * @throws \InvalidArgumentException
     */
    public function __construct($start, $step, $stop)
    {
        $this->start = new DateTime($start instanceof \DateTime ? $start->format('c') : $start);
        $this->step = $step;
        $this->stop = new DateTime($stop instanceof \DateTime ? $stop->format('c') : $stop);
        $this->isInversed = $this->start > $this->stop;

        if ($this->isInfiniteLoop()) {
            throw new \InvalidArgumentException(sprintf('Iterator would be an infinite loop!'));
        }
    }

    /**
     * @param string|DateTime $start
     * @param string $step
     * @param string|DateTime $stop
     * @return static
     */
    public static function create($start, $step, $stop)
    {
        return new static($start, $step, $stop);
    }

    protected function isInfiniteLoop()
    {
        $startTmp = clone $this->start;
        $startTmp->modify($this->step);

        return $this->isInversed
            ? $startTmp > $this->start
            : $startTmp < $this->start;
    }

    public function rewind() {
        $this->iterator = clone $this->start;
        $this->i = 0;
    }

    /**
     * @return bool
     */
    public function valid() {
        return $this->isInversed
            ? $this->iterator >= $this->stop
            : $this->iterator <= $this->stop;
    }

    public function next() {
        $this->iterator->modify($this->step);
        $this->i++;
    }

    /**
     * @return int
     */
    public function key() {
        return $this->i;
    }

    /**
     * @return DateTime
     */
    public function current() {
        return $this->iterator;
    }

    /**
     * @return DateTime[]
     */
    public function getArray()
    {
        $output = array();
        foreach ($this as $date) {
            $output[] = clone $date;
        }

        return $output;
    }

    /**
     * @return string[]
     */
    public function format($format)
    {
        $output = array();
        foreach ($this as $date) {
            $output[] = $date->format($format);
        }

        return $output;
    }
}
